package com.test.tejasj.pharmeasytestapplication.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.test.tejasj.pharmeasytestapplication.Constants;
import com.test.tejasj.pharmeasytestapplication.R;

public class PagerFragment extends Fragment {

	public PagerFragment(){
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		Bundle bundle =  getArguments();
		int imageResourceId = bundle.getInt(Constants.KEY_IMAGE);
		String medicineName = bundle.getString(Constants.KEY_NAME);
		View view = inflater.inflate(R.layout.image_layout, container, false);
		ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
		imageView.setImageResource(imageResourceId);

		TextView textView = (TextView) view.findViewById(R.id.nameText);
		textView.setText(medicineName);
		return view;
	}
}
