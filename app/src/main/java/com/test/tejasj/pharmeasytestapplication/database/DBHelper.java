package com.test.tejasj.pharmeasytestapplication.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.test.tejasj.pharmeasytestapplication.BuildConfig;
import com.test.tejasj.pharmeasytestapplication.Constants;

/**
 * Created by tejasj on 08-05-2016.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final String APP_DB_NAME = "meddb";


    private static final String CREATE_MEDICINE_TABLE  = "CREATE TABLE " +
            Constants.DB_MEDICINE_TABLE_NAME + " (" +
            Constants.DB_COLUMN_ROW_ID + " INTEGER  PRIMARY KEY AUTOINCREMENT ," +
            Constants.DB_COLUMN_ID + " TEXT NOT NULL UNIQUE," +
            Constants.DB_COLUMN_OPRICE + " TEXT NOT NULL," +
            Constants.DB_COLUMN_MRP + " TEXT NOT NULL,"+
            Constants.DB_COLUMN_IMGURL + " TEXT NOT NULL," +
            Constants.DB_COLUMN_PACKSIZELABEL + " TEXT NOT NULL,"+
            Constants.DB_COLUMN_DRUGFORM + " TEXT NOT NULL," +
            Constants.DB_COLUMN_UIP+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_UPRICE+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_AVAILABLE+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_NAME+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_MANUFACTURER+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_PRESCRIPTION_REQUIRED+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_TYPE+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_SLUG+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_PACKFORM+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_FORM+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_DISCOUNT+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_PFORM+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_PROD_FOR_BRAND+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_META+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_HKP_DRUGCODE+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_GENERICS+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_LABEL+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_PACKSIZE+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_MFID+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_SU+ " TEXT NOT NULL," +
            Constants.DB_COLUMN_MAPPED_PFORM+ " TEXT NOT NULL"+");";



    public DBHelper(Context context) {
        super(context, APP_DB_NAME, null, BuildConfig.VERSION_CODE);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_MEDICINE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
