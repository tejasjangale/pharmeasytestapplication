package com.test.tejasj.pharmeasytestapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.test.tejasj.pharmeasytestapplication.Constants;
import com.test.tejasj.pharmeasytestapplication.Fragment.PagerFragment;
import com.test.tejasj.pharmeasytestapplication.R;
import com.test.tejasj.pharmeasytestapplication.database.MyContentProvider;
import com.test.tejasj.pharmeasytestapplication.task.FetchMedListTask;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks{

    ViewPager mViewPager = null;
    MyAdapter mAdapter = null;
    Context mContext = null;
    String[] mProjection = {Constants.DB_COLUMN_NAME};
    ProgressDialog myProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = getApplicationContext();
        mViewPager = (ViewPager) findViewById(R.id.viewpager);

        mAdapter = new MyAdapter(getSupportFragmentManager(), null);

        mViewPager.setAdapter(mAdapter);
        getSupportLoaderManager().initLoader(1, null, this);

        getMedicineRecords();

    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        return new CursorLoader(mContext, MyContentProvider.CONTENT_URI_MEDICINE_TABLE,
                mProjection, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        mAdapter.swapCursor((Cursor) data);
    }

    @Override
    public void onLoaderReset(Loader loader) {
    }


    public static class MyAdapter extends FragmentPagerAdapter {
        Cursor mCursor = null;

        public MyAdapter(FragmentManager fm, Cursor cursor) {
            super(fm);
            mCursor = cursor;
        }

        @Override
        public int getCount() {
            if (mCursor == null)
                return 0;
            else
                return mCursor.getCount();
        }

        @Override
        public Fragment getItem(int position) {

            if (mCursor == null)
                return null;

            mCursor.moveToPosition(position);
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.KEY_IMAGE, R.drawable.dummy);
            bundle.putString(Constants.KEY_NAME, mCursor.getString(mCursor.getColumnIndex(Constants.DB_COLUMN_NAME)));

            PagerFragment pagerFragment = new PagerFragment();
            pagerFragment.setArguments(bundle);

            return pagerFragment;


        }

        public void swapCursor(Cursor c) {
            if (mCursor == c)
                return;

            this.mCursor = c;
            notifyDataSetChanged();
        }

        public Cursor getCursor() {
            return mCursor;
        }
    }


    public static boolean isCursorEmpty(Context context) {

        boolean isEmpty = true;
        Cursor cursor = context.getContentResolver().query(MyContentProvider.CONTENT_URI_MEDICINE_TABLE, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            isEmpty = false;
            cursor.close();

        }
        return isEmpty;
    }

    private boolean isInternetAvailable(Context myCon) {
        ConnectivityManager cm = (ConnectivityManager) myCon
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = null;
        if (cm != null)
            activeNetworkInfo = cm.getActiveNetworkInfo();

        return null != activeNetworkInfo && activeNetworkInfo.isConnected();

    }

    private void getMedicineRecords() {
        if (isInternetAvailable(mContext)) {
            if (isCursorEmpty(mContext)) {
                myProgressDialog = new ProgressDialog(this);
                myProgressDialog.setIndeterminate(true);
                myProgressDialog.setCancelable(false);
                myProgressDialog.setCanceledOnTouchOutside(false);
                myProgressDialog.show();
                myProgressDialog.setMessage(getResources().getString(R.string.Loading));
            }

            FetchMedListTask fetchMedListTask = new FetchMedListTask(mContext, new FetchMedListTask.CallbackInterface() {
                @Override
                public void onTaskCompleted() {
                    if (myProgressDialog != null && myProgressDialog.isShowing()) {
                        myProgressDialog.dismiss();
                    }
                    getSupportLoaderManager().restartLoader(1, null, MainActivity.this);
                }
            });
            fetchMedListTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            Toast.makeText(mContext, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

}
