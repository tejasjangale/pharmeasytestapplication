package com.test.tejasj.pharmeasytestapplication.task;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.test.tejasj.pharmeasytestapplication.Constants;
import com.test.tejasj.pharmeasytestapplication.database.MyContentProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by tejasj on 08-05-2016.
 */
public class FetchMedListTask extends AsyncTask<String, Void, String> {

    public static final String FETCH_LIST_URL = "https://www.1mg.com/api/v1/search/autocomplete?name=b&pageSize=1000000&_=1435404923427";
    Context mContext;
    CallbackInterface mCallbackInterface;
    public interface CallbackInterface {
       void onTaskCompleted();
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    public FetchMedListTask(Context context, CallbackInterface callbackInterface){
        this.mContext = context;
        this.mCallbackInterface = callbackInterface;
    }
    @Override
    protected String doInBackground(String... params) {

        String response = getResponse();
        parseResponse(response);
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        mCallbackInterface.onTaskCompleted();
    }

    private String getResponse() {

        HttpURLConnection connection;
        ByteArrayOutputStream bout;


        String response = null;
        try {
            URL url = new URL(FETCH_LIST_URL);
            connection = (HttpsURLConnection) url.openConnection();

            // Get Response
            InputStream in = connection.getInputStream();

            bout = new ByteArrayOutputStream();
            int len;
            byte[] buffer = new byte[1024];

            while (true) {
                len = in.read(buffer);
                if (len < 0) {
                    break;
                }
                bout.write(buffer, 0, len);
            }
            byte[] data_bytes = bout.toByteArray();
            response = new String(data_bytes, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }


        return response;
    }

    private String parseResponse(String strStatus) {

      String strResponse = "";
        try {
            if(strStatus != null) {
                JSONObject jsonObject = new JSONObject(strStatus);
                /***** "hasMore"Can be used in future optimisation wer we can get list in set of 1000's***/
//               boolean hasMore = jsonObject.getBoolean("hasMore");
//                String statusCode = jsonObject.getString("totalRecordCount");
                JSONArray jsonArray = jsonObject.getJSONArray("result");
                for(int i = 0; i<jsonArray.length(); i++){

                    ContentValues contentValues = new ContentValues();
                    JSONObject medRecordJsonObj = jsonArray.getJSONObject(i);

                    contentValues.put(Constants.DB_COLUMN_PACKSIZELABEL, medRecordJsonObj.getString(Constants.DB_COLUMN_PACKSIZELABEL));
                    contentValues.put(Constants.DB_COLUMN_OPRICE, medRecordJsonObj.getString(Constants.DB_COLUMN_OPRICE));
                    contentValues.put(Constants.DB_COLUMN_MRP, medRecordJsonObj.getString(Constants.DB_COLUMN_MRP));
                    contentValues.put(Constants.DB_COLUMN_IMGURL, medRecordJsonObj.getString(Constants.DB_COLUMN_IMGURL));
                    contentValues.put(Constants.DB_COLUMN_ID, medRecordJsonObj.getString(Constants.DB_COLUMN_ID));
                    contentValues.put(Constants.DB_COLUMN_DRUGFORM, medRecordJsonObj.getString(Constants.DB_COLUMN_DRUGFORM));
                    contentValues.put(Constants.DB_COLUMN_UIP, medRecordJsonObj.getString(Constants.DB_COLUMN_UIP));
                    contentValues.put(Constants.DB_COLUMN_UPRICE, medRecordJsonObj.getString(Constants.DB_COLUMN_UPRICE));
                    contentValues.put(Constants.DB_COLUMN_AVAILABLE, medRecordJsonObj.getString(Constants.DB_COLUMN_AVAILABLE));
                    contentValues.put(Constants.DB_COLUMN_NAME, medRecordJsonObj.getString(Constants.DB_COLUMN_NAME));
                    contentValues.put(Constants.DB_COLUMN_MANUFACTURER, medRecordJsonObj.getString(Constants.DB_COLUMN_MANUFACTURER));
                    contentValues.put(Constants.DB_COLUMN_PRESCRIPTION_REQUIRED, medRecordJsonObj.getString(Constants.DB_COLUMN_PRESCRIPTION_REQUIRED));
                    contentValues.put(Constants.DB_COLUMN_TYPE, medRecordJsonObj.getString(Constants.DB_COLUMN_TYPE));
                    contentValues.put(Constants.DB_COLUMN_SLUG, medRecordJsonObj.getString(Constants.DB_COLUMN_SLUG));
                    contentValues.put(Constants.DB_COLUMN_PACKFORM, medRecordJsonObj.getString(Constants.DB_COLUMN_PACKFORM));
                    contentValues.put(Constants.DB_COLUMN_FORM, medRecordJsonObj.getString(Constants.DB_COLUMN_FORM));
                    contentValues.put(Constants.DB_COLUMN_DISCOUNT, medRecordJsonObj.getString(Constants.DB_COLUMN_DISCOUNT));
                    contentValues.put(Constants.DB_COLUMN_PFORM, medRecordJsonObj.getString(Constants.DB_COLUMN_PFORM));
                    contentValues.put(Constants.DB_COLUMN_PROD_FOR_BRAND, medRecordJsonObj.getString(Constants.DB_COLUMN_PROD_FOR_BRAND));
                    contentValues.put(Constants.DB_COLUMN_META, medRecordJsonObj.getString(Constants.DB_COLUMN_META));
                    contentValues.put(Constants.DB_COLUMN_HKP_DRUGCODE, medRecordJsonObj.getString(Constants.DB_COLUMN_HKP_DRUGCODE));
                    contentValues.put(Constants.DB_COLUMN_GENERICS, medRecordJsonObj.getString(Constants.DB_COLUMN_GENERICS));
                    contentValues.put(Constants.DB_COLUMN_LABEL, medRecordJsonObj.getString(Constants.DB_COLUMN_LABEL));
                    contentValues.put(Constants.DB_COLUMN_PACKSIZE, medRecordJsonObj.getString(Constants.DB_COLUMN_PACKSIZE));
                    contentValues.put(Constants.DB_COLUMN_MFID, medRecordJsonObj.getString(Constants.DB_COLUMN_MFID));
                    contentValues.put(Constants.DB_COLUMN_SU, medRecordJsonObj.getString(Constants.DB_COLUMN_SU));
                    contentValues.put(Constants.DB_COLUMN_MAPPED_PFORM, medRecordJsonObj.getString(Constants.DB_COLUMN_MAPPED_PFORM));

                    Uri uri = mContext.getContentResolver().insert(MyContentProvider.CONTENT_URI_MEDICINE_TABLE, contentValues);
//                    Log.e("insert", uri+"");

                }
                
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return strResponse;
    }

}
