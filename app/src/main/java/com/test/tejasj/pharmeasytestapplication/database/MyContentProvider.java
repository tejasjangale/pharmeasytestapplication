package com.test.tejasj.pharmeasytestapplication.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.test.tejasj.pharmeasytestapplication.Constants;

import java.io.File;

/**
 * Created by tejasj on 08-05-2016.
 */
public class MyContentProvider extends ContentProvider {


    private SQLiteDatabase mDb;

    private static final String AUTHORITY ="com.pharmeasy.provider.MyContentProvider";

    public final static Uri  CONTENT_URI_MEDICINE_TABLE       = Uri.parse("content://"+AUTHORITY+ File.separator + Constants.DB_MEDICINE_TABLE_NAME);

    private static final int MEDICINE_TABLE_OWNER           = 10;

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(AUTHORITY, Constants.DB_MEDICINE_TABLE_NAME, MEDICINE_TABLE_OWNER);
    }

    private static final String MEDICINE_CONTENT_TYPE           = ContentResolver.CURSOR_DIR_BASE_TYPE + File.separator + Constants.DB_MEDICINE_TABLE_NAME;

    @Override
    public boolean onCreate() {
        Context context  = getContext();
        DBHelper mDBHelper = new DBHelper(context);
        mDb           = mDBHelper.getWritableDatabase();
        return (mDb != null);
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder sqLiteQueryBuilder = new SQLiteQueryBuilder();
        Cursor result = null;

        switch (sURIMatcher.match(uri))
        {
            case MEDICINE_TABLE_OWNER:
            {
                sqLiteQueryBuilder.setTables(Constants.DB_MEDICINE_TABLE_NAME);
                result  = sqLiteQueryBuilder.query(mDb,projection,selection,selectionArgs,null,null,sortOrder);
                result.setNotificationUri(getContext().getContentResolver(), uri);
                getContext().getContentResolver().notifyChange(uri,null);

            }
            break;



        }

        return result;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        long rowId;
        switch (sURIMatcher.match(uri))
        {

            case MEDICINE_TABLE_OWNER:
            {
                rowId = mDb.insertWithOnConflict(Constants.DB_MEDICINE_TABLE_NAME,null,values,SQLiteDatabase.CONFLICT_IGNORE);
                if (rowId > 0)
                {
                    Uri insertedUri = ContentUris.withAppendedId(CONTENT_URI_MEDICINE_TABLE, rowId);
                    getContext().getContentResolver().notifyChange(insertedUri,null);
                    return insertedUri;
                }
                return  null;
            }

        }

        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        int insertCount = 0;
        if(values.length == 0)
            return 0;
        switch (sURIMatcher.match(uri))
        {

            case MEDICINE_TABLE_OWNER:
            {
                try {
                    mDb.beginTransaction();
                    for (ContentValues contentValues : values)
                    {
                        long id = mDb.insertWithOnConflict(Constants.DB_MEDICINE_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
                        if(id >0)
                        {
                            insertCount++;
                            Uri insertedUri = ContentUris.withAppendedId(CONTENT_URI_MEDICINE_TABLE,id);
                            getContext().getContentResolver().notifyChange(insertedUri,null);
                        }
                    }
                    mDb.setTransactionSuccessful();
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally
                {
                    mDb.endTransaction();
                }

                return  insertCount;
            }



        }

        return insertCount;
    }
}
