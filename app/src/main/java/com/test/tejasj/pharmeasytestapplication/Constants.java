package com.test.tejasj.pharmeasytestapplication;

/**
 * Created by tejasj on 08-05-2016.
 */
public class Constants {


    public static final String DB_MEDICINE_TABLE_NAME = "medicine";

    public static final String DB_COLUMN_ROW_ID = "_id";
    public static final String DB_COLUMN_PACKSIZELABEL = "packSizeLabel";
    public static final String DB_COLUMN_OPRICE = "oPrice";
    public static final String DB_COLUMN_MRP = "mrp";
    public static final String DB_COLUMN_IMGURL = "imgUrl";
    public static final String DB_COLUMN_ID = "id";
    public static final String DB_COLUMN_DRUGFORM = "drug_form";
    public static final String DB_COLUMN_UIP = "uip";
    public static final String DB_COLUMN_UPRICE = "uPrice";
    public static final String DB_COLUMN_AVAILABLE = "available";
    public static final String DB_COLUMN_NAME = "name";
    public static final String DB_COLUMN_MANUFACTURER = "manufacturer";
    public static final String DB_COLUMN_PRESCRIPTION_REQUIRED = "prescriptionRequired";
    public static final String DB_COLUMN_TYPE = "type";
    public static final String DB_COLUMN_SLUG = "slug";
    public static final String DB_COLUMN_PACKFORM = "packForm";
    public static final String DB_COLUMN_FORM = "form";
    public static final String DB_COLUMN_DISCOUNT = "discountPerc";
    public static final String DB_COLUMN_PFORM = "pForm";
    public static final String DB_COLUMN_PROD_FOR_BRAND = "productsForBrand";
    public static final String DB_COLUMN_META = "meta";
    public static final String DB_COLUMN_HKP_DRUGCODE = "hkpDrugCode";
    public static final String DB_COLUMN_GENERICS = "generics";
    public static final String DB_COLUMN_LABEL = "label";
    public static final String DB_COLUMN_PACKSIZE = "packSize";
    public static final String DB_COLUMN_MFID = "mfId";
    public static final String DB_COLUMN_SU = "su";
    public static final String DB_COLUMN_MAPPED_PFORM = "mappedPForm";

    public static final String KEY_IMAGE= "image";
    public static final String KEY_NAME= "name";



}
